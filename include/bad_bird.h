#ifndef BAD_BIRD_IS_DEF
#define BAD_BIRD_IS_DEF

#include "game.h"
#include "object.h"

typedef Object BadBird;

void badbird_class_init(object_class_t* class);

#endif
