#ifndef CONSTANTS_IS_DEF
#define CONSTANTS_IS_DEF

#define MAP_WIDTH 30
#define MAP_HEIGHT 24

#define TILE_SIZE 64

/**
 * The width of the main window.
 */
#define WIN_WIDTH 1280

/**
 * The height of the main window.
 */
#define WIN_HEIGHT 640

/**
 * The default name for maps.
 */
#define DEFAULT_MAP_NAME "default.map"

#define MIN_BORDER_DISTANCE 64
#define MARGIN TILE_SIZE * 4

#define DEAD_DELAY 5000  // ms

#define MAX_SPEED 32

#endif
