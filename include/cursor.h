#ifndef CURSOR_IS_DEF
#define CURSOR_IS_DEF

#include "game.h"
#include "object.h"

typedef Object Cursor;

void cursor_class_init(object_class_t* class);

#endif