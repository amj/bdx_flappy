
#ifndef DEBUG_IS_DEF
#define DEBUG_IS_DEF

// La fonction PRINT_DEBUG permet d'afficher selectivement certains messages
// de debug. Le choix du type des messages s'effectue au moyen d'une
// chaine contenant des filtres. Ces filtres sont :
//
//      '+' -- active tous les messages de debug
//      'd' -- opérations ayant trait au disque (chargement/sauvegarde de map)
//      'm' -- gestion mémoire
//      'g' -- opérations graphiques
//      'a' -- routines d'animation
//      't' -- gestion des temporisateurs
//      'c' -- gestion des collisions
//      'p' -- information de profiling
//      's' -- gestion sur smartphones

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void debug_init(char *flags);
int debug_enabled(char flag);

extern char *debug_flags;

/**
 * Print a the formated string if a debug flag is enabled.
 */
static inline void PRINT_DEBUG(char flag, char *format, ...) {
    if (debug_flags != NULL && debug_enabled(flag)) {
        va_list ap;

        va_start(ap, format);
        vfprintf(stderr, format, ap);
        va_end(ap);
    }
}

/**
 * If the condition bool is strictly positive and the debug flag is enabled,
 * print a the formated string and exit.
 */
static inline void DEBUG_ERROR(int bool, char flag, char *format, ...) {
    if (bool && (debug_flags != NULL) && debug_enabled(flag)) {
        PRINT_DEBUG(flag, format);
        exit(-1);
    }
}

#endif
