#ifndef EXPLOSION_IS_DEF
#define EXPLOSION_IS_DEF

#include "game.h"
#include "object.h"

typedef Object Explosion;

void explosion_class_init(object_class_t* class);

#endif
