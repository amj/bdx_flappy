#ifndef GAME_IS_DEF
#define GAME_IS_DEF

#include <stdlib.h>
#include "SDL.h"

#include "error.h"
#include "graphics.h"
#include "list.h"
#include "map.h"
#include "object.h"
#include "object_elem.h"

/** @defgroup Game_group Game class
 *  @{
 */

/** @private
 * The current state of a Game
 */
typedef enum {
    GAME_STATE_DESTROYED,
    GAME_STATE_INIT,
    GAME_STATE_STARTED,
    GAME_STATE_EDITOR,
    // GAME_STATE_END,
    GAME_STATE_PAUSED
} game_state;

/**
 * Represents a Game with a state and a cycle counter.
 *
 * Only use the @ref Game_group method's to modify the Game's state, Game's
 * attributes are private.
 */
typedef struct Game {
    game_state state;        ///< @private
    long cycle_nb;           ///< @private // NOTE: Yes long is big enough
    SDL_Point position;      ///< @private
    Object *visible_object;  ///< @private
    Graphics *graphics;      ///< @private
    struct list_head objects_list;  ///< @private
    Map *map;                       ///< @private
} Game;

/**
 * Iterate over every element of the list of Objects of the Game.
 * @param game The Game which contains Object.
 * @param obj_e the element cursor.
 */
#define for_each_objects(game, obj_e) \
    list_for_each_entry_safe(ObjectElem, obj_e, &game->objects_list, listp)

/**
 * Iterate over every element of the list of Objects of the Game.
 * @param game The Game which contains Object.
 * @param prev_e the previous element of the start.
 * @param obj_e the element cursor.
 */
#define for_each_objects_after(game, prev_e, obj_e)           \
    list_for_each_entry_safe_after(ObjectElem, obj_e, prev_e, \
                                   &game->objects_list, listp)

/**
 * Initialises a Game.
 */
void Game__init(Game *game, Uint32 render_flags, char *background_skin, char *map);

static inline Game *Game__new(Uint32 render_flags, char *background_skin, char *map) {
    Game *game = calloc(1, sizeof(Game));
    if (game != NULL) Game__init(game, render_flags, background_skin, map);
    return game;
}

/**
 * Removes elements of the list of Object.
 * Puts the game in the state GAME_STATE_DESTROYED.
 */
void Game__clean(Game *game);

static inline void Game__delete(Game *game) {
    Game__clean(game);
    free(game);
}

/**
 * Puts the Game in the state started if the curent state is init.
 * return 0 if the state was init -1 otherwise.
 */
static inline int Game__start(Game *game) {
    if (game->state == GAME_STATE_INIT)
        game->state = GAME_STATE_STARTED;
    else
        return -1;
    return 0;
}

/**
 * Check if the game is started.
 */
static inline int Game__is_started(Game *game) {
    return game->state == GAME_STATE_STARTED;
}

/**
 * Check if the game is in editor.
 */
static inline int Game__is_editor(Game *game) {
    return game->state == GAME_STATE_EDITOR;
}

/**
 * Check if the game is in init state.
 */
static inline int Game__is_init(Game *game) {
    return game->state == GAME_STATE_INIT;
}

/**
 * Toggle the status between pause and started.
 * @return -1 if the game was not stated or paused.
 */
static inline int Game__toggle_pause(Game *game) {
    if (game->state == GAME_STATE_STARTED) {
        game->state = GAME_STATE_PAUSED;
        for_each_objects(game, obj_e) Object__pause(obj_e->obj);
    } else if (game->state == GAME_STATE_PAUSED) {
        game->state = GAME_STATE_STARTED;
        for_each_objects(game, obj_e) Object__unpause(obj_e->obj);
    } else
        return -1;
    return 0;
}

/**
 * Increments the Game cycle counter.
 *
 * @return the current cycle number after incrementation if the game is stated,
 * -1 otherwise.
 */
static inline long Game__incr_cycle(Game *game) {
    if (game->state == GAME_STATE_STARTED || game->state == GAME_STATE_EDITOR) return ++(game->cycle_nb);
    return -1;
}


/**
 * Get coordinates of the origin.
 */
static inline void Game__get_orig(Game *game, int *x, int *y) {
    *x = game->position.x;
    *y = game->position.y;
}

/**
 * Get the graphics attribute of the Game
 */
static inline Graphics *Game__get_Graphics(Game *game) {
    return game->graphics;
}

/**
 * Get the map of the game
 */
static inline Map *Game__get_map(Game *game) {
    return game->map;
}

/**
 * Add an Object in the Game.
 */
static inline void Game__add_Object(Game *game, Object *object) {
    ObjectElem *elem = ObjectElem__new(object, &game->objects_list);
    if (elem == NULL) exit_with_error("ObjectElem__new");
}

/**
 * Animates every Objects of the game.
 * @param up the up key is pressed in the new cycle.
 * @param down the down key is pressed in the new cycle.
 */
long Game__next_cycle(Game *game, int keys);

/**
 * Render the game on its window in the current state.
 */
void Game__render(Game *game);

/**
 * Renders every Objectsof the Game.
 */
void Game__render_objects(Game *game);

/**
 * Toggle the status between editor and started.
 * @return -1 if the game was not stated or in editor.
 */
int Game__toggle_editor(Game *game);

/** @} */  // end of group

#endif
