#ifndef GRAPHICS_IS_DEF
#define GRAPHICS_IS_DEF

#include <SDL.h>

//#include "game.h"
//#include "sprite.h"
#include "object.h"
typedef struct Game Game;
typedef struct Sprite Sprite;

#include "static_object.h"

#define GFX_NONE 0
#define GFX_BLINK 1
#define GFX_FADE 2
#define GFX_CENTER 4
#define GFX_SCALE 8
#define GFX_ROTATE 16

/** @defgroup Graphics_group Graphics class
 *  @{
 */

/// @private
typedef enum {
    GRAPHICS_STATE_UNINITIALIZED,
    GRAPHICS_STATE_INIT,
    GRAPHICS_STATE_DESTROYED
} graphics_state;

/**
 * Class containing the data needed to displays elements on screen.
 *
 * Attributes are private. @ref Graphics_group
 */
typedef struct Graphics {
    graphics_state state;                                  ///< @private
    SDL_Renderer *ren;                                     ///< @private
    SDL_Window *win;                                       ///< @private
    SDL_Texture *background;                               ///< @private
    SDL_Texture *tree[3];                                  ///< @private
    Sprite *sprites[__OBJECT_TYPE_NUM];                    ///< @private
    Sprite *static_sprites[__STATIC_OBJECT_TYPE_NUM - 1];  ///< @private
} Graphics;

/**
 * Initializes the main window and a renderer.
 */
void Graphics__init(Graphics *graphics, Uint32 render_flags,
                    char *background_skin);

static inline Graphics *Graphics__new(Uint32 render_flags,
                                      char *background_skin) {
    Graphics *graphics = calloc(1, sizeof(Graphics));
    if (graphics != NULL)
        Graphics__init(graphics, render_flags, background_skin);
    return graphics;
}

/**
 * Destroys the renderer, the window and textures.
 */
void Graphics__clean(Graphics *graphics);

static inline void Graphics__delete(Graphics *graphics) {
    Graphics__clean(graphics);
    free(graphics);
}

static inline void Graphics__get_size(Graphics *graphics, int *x, int *y) {
    SDL_GetRendererOutputSize(graphics->ren, x, y);
}

/**
 * Renders the game in its current state.
 */
void Graphics__render(Graphics *graphics, Game *game, int map_height);

/**
 * Renders an object on the main window at a given coordinate.
 *
 * @param seq_number The index number of the image of the Sprite. Used to
 * animate the Object.
 */
void Graphics__render_object(Graphics *graphics, object_type type,
                             SDL_Rect *dst, int obj_cycle, int direction,
                             Uint8 alpha);

void Graphics__render_static_object(Graphics *graphics, static_object_type type,
                                    int dst_x, int dst_y, int obj_cycle,
                                    int object_direction, Uint8 alpha);

Sprite *Graphics__get_sprite(Graphics *graphics, object_type type);

/** @} */  // end of group
#endif
