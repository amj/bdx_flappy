#ifndef KEYS_IS_DEF
#define KEYS_IS_DEF

typedef enum {
    KEY_NONE = 0,
    KEY_SPACE = 1,
    KEY_UP = 2,
    KEY_DOWN = 4,
    KEY_LEFT = 8,
    KEY_RIGHT = 16,
    KEY_TAB = 32
} key_type;

#endif
