#ifndef MAP_IS_DEF
#define MAP_IS_DEF

#include "graphics.h"
#include "static_object.h"

enum {
    MAP_OBJECT_SOLID,
    MAP_OBJECT_SEMI_SOLID,
    MAP_OBJECT_AIR,
    MAP_OBJECT_NUM
};

typedef struct Map Map;

Map* Map__new(unsigned width, unsigned height);

int Map__is_valid(Map* map, unsigned x, unsigned y);

void Map__init(Map* map, unsigned width, unsigned height);

void Map__clean(Map* map);

void Map__delete(Map* map);

void Map__create(Map* map);

void Map__render(Map* map, Graphics* graphics, int x_orig, int y_orig,
                 int game_cycle);

void Map__set(Map* map, static_object_type map_object, unsigned x, unsigned y);

static_object_type Map__get(Map* map, unsigned x, unsigned y);

static_object_type Map__safe_get(Map* map, unsigned x, unsigned y);

void Map__get_rect(Map* map, SDL_Rect* rect);

Map* Map__load(char* filename);

int Map__save(Map* map, char* filename);

#endif
