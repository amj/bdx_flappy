#ifndef MISSILE_IS_DEF
#define MISSILE_IS_DEF

#include "game.h"
#include "object.h"

typedef Object Missile;

void missile_class_init(object_class_t* class);

#endif
