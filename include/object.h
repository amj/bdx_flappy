#ifndef OBJECT_IS_DEF
#define OBJECT_IS_DEF
#include <SDL.h>
#include "constants.h"
#include "timer.h"

typedef struct Game Game;

//#include "graphics.h"
typedef struct Graphics Graphics;

typedef struct Map Map;

/** @defgroup Object_group Object class
 *  @{
 */

/**
 * Possible types of an Object.
 */
typedef enum {
    OBJECT_TYPE_MARIO,      ///< bird
    OBJECT_TYPE_BAD_BIRD,   ///< bad bird
    OBJECT_TYPE_MISSILE,    ///< missile
    OBJECT_TYPE_EXPLOSION,  ///< explosion
    OBJECT_TYPE_EDITOR,     ///< editor cursor
    // OBJECT_TYPE_TEXT,
    __OBJECT_TYPE_NUM  ///< @private
} object_type;

/** @private
 * Possible states of an Object.
 */
typedef enum {
    OBJECT_STATE_NORMAL,
    OBJECT_STATE_IN_AIR,
    OBJECT_STATE_ON_GROUND,
    // OBJECT_STATE_DEAD, // can be dead and on ground
    OBJECT_STATE_DESTROYED
} object_state;

/**
 * Respresents an Object which can be displayed in the Game.
 *
 * Only use the @ref Object_group method's to modify the Object's state,
 * Object's attributes are private.
 */
typedef struct Object {
    object_type type;       ///< @private can be OBJECT_TYPE_BIRD, ...
    void *parent;           ///< @private
    object_state state;     ///< @private can be OBJECT_STATE_NORMAL, ...
    int is_dead;            ///< @private
    struct SDL_Point size;  ///< @private size in number of blocks
    struct SDL_Point position;
    struct SDL_Point speed;
    int direction;  ///< @private 0 = left, 1 = right
    int cycle;      ///< @private the current image in the sequence
    int buff;       ///< @private
    TimerId timer;  ///< @private
} Object;

typedef int (*animate_func_t)(Object *obj, Game *game);            ///< @private
typedef int (*bool_func_t)(Object *obj);                           ///< @private
typedef Object *(*collision_func_t)(Object *obj, Object *other);   ///< @private
typedef void (*action_func_t)(Object *obj, Game *game, int keys);  ///< @private
typedef int (*sequence_func_t)(Object *obj, int nb_images);        ///< @private
typedef void (*timer_func_t)(Object *obj);                         ///< @private

/// @private
typedef struct {
    char *class_name;  // for debug
    animate_func_t animate_onestep_func;
    action_func_t action_func;
    timer_func_t timer_func;
    bool_func_t is_solid_func;
    collision_func_t collision_func;
} object_class_t;

/// @private
extern object_class_t object_classes[];

/// @private
static inline void object_class_init(object_class_t *class) {
    class->class_name = "Object";
    class->animate_onestep_func = NULL;
    class->action_func = NULL;
    class->timer_func = NULL;
    class->is_solid_func = NULL;
    class->collision_func = NULL;
}

static inline void Object__get_dst(Object *object, SDL_Rect *dst) {
    dst->w = object->size.x * TILE_SIZE;
    dst->h = object->size.y * TILE_SIZE;
    dst->x = object->position.x;
    dst->y = object->position.y;
}

/**
 * Get the name of the object's class.
 */
static inline char *Object__get_name(Object *object) {
    return object_classes[object->type].class_name;
}

/**
 * Initialize the object_class array for polymophic methods
 */
void object_classes_init(void);

/**
 * Initialize fields of object obj
 */
void Object__init(Object *object, void *parent, object_type type, int x, int y,
                  int xs, int ys, int x_size, int y_size, int direction);

static inline Object *Object__new(void *parent, object_type type, int x, int y,
                                  int xs, int ys, int x_size, int y_size,
                                  int direction) {
    Object *object = calloc(1, sizeof(Object));
    if (object != NULL)
        Object__init(object, parent, type, x, y, xs, ys, x_size, y_size,
                     direction);
    return object;
}

int Object__is_out(Object *object, Map *map);

/**
 * Animate the object
 * @return true if the object have to be destruct
 */
static inline int Object__animate(Object *object, Game *game) {
    animate_func_t fun = object_classes[object->type].animate_onestep_func;
    if (fun != NULL) return fun(object, game);
    return 0;
}

/**
 * Run Object's actions.
 */
static inline void Object__action(Object *object, Game *game, int keys) {
    action_func_t fun = object_classes[object->type].action_func;
    if (fun != NULL) fun(object, game, keys);
}

/**
 * This function have to be call when the timer expires.
 */
static inline void Object__timer_expires(Object *object) {
    timer_func_t fun = object_classes[object->type].timer_func;
    if (fun != NULL) fun(object);
}

/**
 * Check if an object is solid or not
 */
static inline int Object__is_solid(Object *object) {
    bool_func_t fun = object_classes[object->type].is_solid_func;
    if (fun != NULL) return fun(object);
    return 0;
}

/**
 * Collision
 *
 * @return 1 if the object have to be removed.
 */
static inline Object *Object__collision(Object *object, Object *other) {
    collision_func_t fun = object_classes[object->type].collision_func;
    if (fun != NULL) return fun(object, other);
    return NULL;
}

/// @private inline just to avoid warnings :D
static inline int Object__true(Object *obj) { return 1; }
static inline int Object__false(Object *obj) { return 0; }

/**
 * Cancels the timer associated to the Object.
 */
static inline void Object__timer_cancel(Object *object) {
    timer_cancel(object->timer);
}

/**
 * Add a new timer, cancel the existing timer if it exist.
 * @param delay the time before that the timer expires.
 */
void Object__timer_add(Object *object, Uint32 delay);

/**
 * Disables the timer if the Object is in state dead.
 */
static inline void Object__pause(Object *object) {
    if (object->is_dead) Object__timer_cancel(object);
}

/**
 * Restarts the timer if needed.
 */
static inline void Object__unpause(Object *object) {
    if (object->is_dead)
        Object__timer_add(object,
                          DEAD_DELAY / 2);  // can't get the elapsed time
}

/**
 * get the type of the Object.
 */
static inline object_type Object__get_type(Object *object) {
    return object->type;
}

static inline void Object__get_coordinates(Object *object, int *x, int *y) {
    if (x != NULL) *x = object->position.x;
    if (y != NULL) *y = object->position.y;
}

/**
 * Destroys the Object.
 */
static inline void Object__destruct(Object *object) {
    object->state = OBJECT_STATE_DESTROYED;
    Object__timer_cancel(object);
}

static inline void Object__delete(Object *object) {
    Object__destruct(object);
    free(object);
}

/**
 * Add this Object to the Game's renderder.
 */
void Object__render(Object *object, Game *game);

/** @} */  // end of group

#endif
