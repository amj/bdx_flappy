#ifndef OBJECT_ELEM_IS_DEF
#define OBJECT_ELEM_IS_DEF

#include "error.h"
#include "list.h"
#include "object.h"

/** @defgroup ObjectElem ObjectElem class
 *  @{
 */

/**
 * An element of a list containing an Object.
 */
typedef struct ObjectElem {
    struct list_head listp;  ///< Pointers to previous and next elements.
    Object* obj;             ///< The Object contained in this element.
} ObjectElem;

/** @ingroup ObjectElem
 * Creates and ObjectElem and add in a list.
 * @param prev the previous element in the list.
 */
static inline void ObjectElem__init(ObjectElem* objectElem, Object* object,
                             struct list_head* prev) {
    list_add(&objectElem->listp, prev);
    objectElem->obj = object;
}

static inline ObjectElem* ObjectElem__new(Object* object,
                                          struct list_head* prev) {
    ObjectElem* elem = calloc(1, sizeof(ObjectElem));
    if (elem != NULL) ObjectElem__init(elem, object, prev);
    return elem;
}

/** @ingroup ObjectElem
 * Removes an element from its list.
 */
static inline void ObjectElem__remove(ObjectElem* objectElem) {
    list_del(&objectElem->listp);
}

static inline void ObjectElem__delete(ObjectElem* objectElem) {
    ObjectElem__remove(objectElem);
    free(objectElem->obj);
    free(objectElem);
}

/** @} */  // end of group

#endif
