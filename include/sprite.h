#ifndef SPRITE_IS_DEF
#define SPRITE_IS_DEF

#include <SDL.h>
#include "error.h"

/** @defgroup Sprite_group Sprite class
 *  @{
 */

typedef enum sprite_anim_type {
    SPRITE_ANIM_SIMPLE,
    SPRITE_ANIM_YOYO,
    __SPRITE_ANIM_TYPE_NUM  ///< @private
} sprite_anim_type;

/**
 * Informations associated to Object's texture.
 *
 * See @ref Sprite_group
 */
typedef struct Sprite {
    SDL_Texture *texture;  ///< @private
    char *text_path;       ///< @private debug

    sprite_anim_type type;  ///< @private

    struct SDL_Point native_size; ///< @private size of the complete image
    struct SDL_Point element_size; ///< @private size of an element of the sprite in the image/texture

    unsigned nb_images;  ///< @private
                         // number of images in the animation

    int original_direction;  ///< @private

} Sprite;

/**
 * Initializes The Sprite with the associated texture.
 */
void Sprite__init(Sprite *sprite, SDL_Texture *texture, char *text_path,
                  sprite_anim_type type, int width, int height, int nb_images,
                  int direction);

static inline Sprite *Sprite__new(SDL_Texture *texture, char *text_path,
                                  sprite_anim_type type, int width, int height,
                                  int nb_images, int direction) {
    Sprite *sprite = calloc(1, sizeof(Sprite));
    if (sprite != NULL)
        Sprite__init(sprite, texture, text_path, type, width, height, nb_images,
                     direction);
    return sprite;
}

/**
 * Destroys the Associated texture.
 */
void Sprite__clean(Sprite *sprite);

static inline void Sprite__delete(Sprite *sprite) {
    Sprite__clean(sprite);
    free(sprite);
}

static inline SDL_Texture *Sprite__get_texture(Sprite *sprite) {
    return sprite->texture;
}


void Sprite__render(Sprite *sprite, SDL_Renderer *ren, SDL_Rect *dst,
                    int obj_cycle, int object_direction, Uint8 alpha);

/** @} */  // end of group
#endif
