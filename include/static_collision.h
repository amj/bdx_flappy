#ifndef STATIC_COLLISION_IS_DEF
#define STATIC_COLLISION_IS_DEF
#include "game.h"

/**
 * checks for collisions with static objects bellow something
 */
char checkLowerCollision(int x, int y, int objectWidth, int objectHeight, Game* game);

/**
 * checks for collisions with static objects above something
 */
char checkUpperCollision(int x, int y, int objectWidth, Game* game);

/**
 * checks for collisions with static objects to the right of something
 */
char checkRightCollision(int x, int y, int objectWidth, int objectHeight, Game* game);

/**
 * checks for collisions with static objects to the left of something
 */
char checkLeftCollision(int x, int y, int objectHeight, Game* game);

#endif