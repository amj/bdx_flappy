#ifndef STATIC_OBJECT_IS_DEF
#define STATIC_OBJECT_IS_DEF

typedef enum {
    STATIC_OBJECT_EMPTY = 0,
    STATIC_OBJECT_WALL,
    STATIC_OBJECT_PLATFORM,
    STATIC_OBJECT_WATER,
    STATIC_OBJECT_FLOWER,
    STATIC_OBJECT_FLOWER2,
    STATIC_OBJECT_GRASS,
    STATIC_OBJECT_GROUND,
    STATIC_OBJECT_HERB,
    STATIC_OBJECT_COIN,
    __STATIC_OBJECT_TYPE_NUM
} static_object_type;

typedef enum {
    STATIC_PROPERTY_NONE = 0,
    STATIC_PROPERTY_SOLID = 1,
    STATIC_PROPERTY_DESTRUCTIBLE = 2,
    STATIC_PROPERTY_JUMP_THROUGH = 4,
    STATIC_PROPERTY_BOOST_UP = 8
} static_object_property;

static int object_properties[__STATIC_OBJECT_TYPE_NUM] = {
    STATIC_PROPERTY_NONE,          // empty
    STATIC_PROPERTY_SOLID,         // wall
    STATIC_PROPERTY_JUMP_THROUGH,  // floor
    STATIC_PROPERTY_BOOST_UP,      // water
    STATIC_PROPERTY_NONE,          // flower
    STATIC_PROPERTY_NONE,          // flower2
    STATIC_PROPERTY_NONE,          // grass
    STATIC_PROPERTY_SOLID,         // ground
    STATIC_PROPERTY_NONE,          // herb
    STATIC_PROPERTY_NONE           // coin

};

static inline int static_object__get_properties(static_object_type type) {
    return object_properties[type];
}

static inline int static_object__has_property(static_object_type type,
                                              static_object_property property) {
    return (object_properties[type] & property) != 0;
}

#endif
