#ifndef TIMER_IS_DEF
#define TIMER_IS_DEF
#include <stdint.h>

#include "SDL.h"

typedef SDL_TimerID TimerId;

enum { TIMER_OBJECT_EXP };

/** @defgroup TimerId
 *  @{
 */

static Uint32 timer_callback(Uint32 interval, void* param) {
    SDL_Event event;
    SDL_UserEvent userevent;

    userevent.type = SDL_USEREVENT;
    userevent.code = TIMER_OBJECT_EXP;
    userevent.data1 = param;
    userevent.data2 = NULL;

    event.type = SDL_USEREVENT;
    event.user = userevent;

    SDL_PushEvent(&event);
    return 0;
}

static inline int timer_init(void) { return 0; }

static inline TimerId Timer_set(Uint32 delay, void* param) {
    return SDL_AddTimer(delay, timer_callback, param);
}

static inline int timer_cancel(TimerId timer_id) {
    return SDL_RemoveTimer(timer_id);
}


/** @} */ // end of group

#endif
