# Battlefield Mario    {#mainpage}

# Introduction
Battlefield Mario est un jeu développé dans le cadre du cours projet technologique de Licence 3 d'informatique à l'Université de Bordeaux.
Il s'agit d'un jeu de plateforme 2D écrit en C.

Les consignes du projet peuvent être consultées [ici](https://gforgeron.gitlab.io/battlefieldmario/).

> note: _pour générer ce document lancez à la racine du projet :_
> ```shell
> doxygen
> ```


# Compiler et lancer
Pour compiler lancez la commande `make` à la racine du projet. Avant de lancer le jeu créez les liens symboliques vers les dossiers `sons/` et `images/` dans le dossier parent de la racine du projet. Lancez le jeu avec `./game` à la racine du projet.

# Jouer
Lorsque le jeu démarre, celui-ci est en pause, les touches \<espace\> et \<flèche du haut\> permettent de commencer à jouer. Appuyer sur \<espace\> permet de mettre en pause le jeu. Appuyer sur \<flèche du bas\> permet de lancer un missile avec le personnage principal (actuellement l'oiseau).

Lorsque le personnage touche le haut de l'écran, celui-ci passe dans l'état DEAD pendant 5 secondes. Si une pause est acctionnée pendant que l'état DEAD est activé, à la fin de la pause celui-ci est maintenu une durée arbitraire après la fin de la pause. (actuellement 2.5 secondes)

# Découpage du code

## Game
Cette structure contient l'état du jeu : d'une part, l'avancement en x et en y de la fenêtre, d'autre part la liste des Object qui doivent y être réprésentés. À chaque tour du jeu la boucle principale du main passe les actions effectuées par l'utilisateur et déclenche un nouveau tour via la fonction Game__next_cycle(), cela met à jour l'ensemble des Object du jeu.

Après avoir déclenché les différentes actions des Object (dans le cycle précédent) la boucle main appelle Game__render() qui après avoir nettoyé l'affichage affiche l'ensemble des Object à l'écran.


## Graphics
Regroupe l'ensemble des informations nécessaire à l'affichage. Notamment les Sprite, les textures, la fenêtre et son renderer associé. Rassembler ces éléments dans une structure et ainsi éviter d'avoir un lien direct entre les Object et le rendu graphique permet de facilement adapter le code afin de changer de thème pendant l'exécution ou de changer de fenêtre, afficher le rendu sur plusieurs fenêtres etc... Pour cela il est juste nécessaire de respecter l'interface de _graphics.h_.

Les Sprite (et les textures associées) sont créés par Graphics la première fois que le rendu de l'Object est effectué, Graphics possède les informations associées aux textures dans un tableau statique texture_info. Pour gérer différents thèmes d'affichage une solution serait d'ajouter un fichier de configuration en parramètre à Graphics__init().

## Sprite
Regroupe les différentes informations liées aux Textures des Object : les dimentions de la texture, le nombre d'images, la taille d'une image. L'animation est entièrement générée par le Sprite, le type d'animation (simple ou yoyo) est défini lors de l'initialisation via une énumération.


## Object
Regroupe l'état d'un Object affiché dans le jeu. Différentes actions peuvent être effectuées par l'Object selon sa classe. 


Pour chaque type d'Object, la table des méthodes associées est initialisée par \<object_type\>_class_init(), cela évite de mettre les fonctions associées aux différents type d'Object dans les header et permet de s'assuré qu'elle sont appelées uniquement sur les bons objets.


# Bilan des fonctionnalités
À l'exception du générateur d'enemis qui ne correspond pas 100% à la consigne, l'ensemble des consignes ont été implémentées.

## Défilement de l'arrière-plan
Celui-ci est calculé à partir des champs x_orig et y_orig de Game. Ceux-ci sont modifiés par le personnage principal (l'oiseau) lorsqu'il se déplace. (à l'appel de Object__action() )

## Animation du Sprite
La gestion de l'animation du Sprite se fait actuellement grace à un compteur de cycles dans les Object. Cela évite de se soucier du nombre d'images du Sprite dans l'Object. Cela permet entre-autres de facilement changer pendant l'exécution le Sprite associé à un Object si cela était demandé.

## Affichage des Object
Pour déterminer l'emplacement de chaque Object sur la fenêtre, les coordonnées du jeu x_orig et y_orig sont déduites des coordonnées absolues contenues dans l'Object. l'affichage se fait à partir des coordonnées relatives. Cela permet que lorsque la vitesse absolue de l'explosion tend vers 0, sa vitesse par rapport à l'oiseau qui vole est négative (elle recule donc vers la gauche).

## Générateur d'enemis
Ne correspond pas vraiment à la consigne, écrit rapidement pour pouvoir jouer.

## Gestion des collisions
La gestion des collisions est implémentée en deux parties, d'une part la détection effectuée à chaque cycle, d'autre part le comportement adopté par chaque Object. 
Pour chaque Object où il y a collision la méthode Object__collision() est appliquée ou le second paramètre est le second Object. la méthode appelle alors Object__is_solid() afin de savoir si l'Object peut être traversé. Cela permet d'adapter les intéractions en fonction de l'état actuel de l'Object. Afin qu'un Object ne puisse pas se collisioner avec celui qui l'a lancé, un champ parent a été ajouté à la structure Object.


# Points délicats

Je n'ai pour m'a part pas relevé de point particulièrement plus compliqué que les autres excepté peut-être l'organisation.

## Organisation du projet
Une des difficultés pouvait être de "s'emêler les pinceaux" en modifiant les variables globales et leurs champs depuis n'importe quel endroit du code.

Afin d'améliorer la maintenabilité du code, les adaptations suivantes ont été utilisées :
- Utilisation d'une arborescence de structures plutôt que des variables globales _extern_.
- Utilisation de CamelCase pour les noms de structures et snake_case pour les noms de fonction.
- Encapsulation : les fonctions ne modifient directement que la structure à laquelle elles sont associées, par convention leur nom commence par le nom de cette structure, il s'agit également du type de leur premier argument. Pour modifier un champ d'une autre structure, une fonction fait appel à une fonction associée à cette structure.
exemple :

La méthode Object__animate(Object *object, Game *game) est associée à la structure Object, elle ne modifiera donc directement aucun champ de la structure de type Game.

Cela a pour concéquence de multiplier les méthodes nécessaires et donc les appels de fonction. Pour cette raison, un grand nombre de fonction son _inline_, cette abstraction a donc un coût nul à l'éxécution.


# Limitations

## Incohérences de l'impléméntation
La gestion des timer n'a pas vraiment de sens car ceux-ci se basent sur le temps alors que le reste de la logique du jeu se base sur la vitesse du rendu graphique. Une adaptation permettant de corriger cela a été implémentée dans la branche render_thread et est détaillée dans la section sur les améliorations possibles.

## Un potentiel overflow après plus d'un an
Chaque Object possède un compteur de cycle qui est incrémenté (p-en pour l'oiseau) quand celui-ci est en l'air. Il n'y a pas de soucis d'overflow car à moins de 50 cycles par seconde, il faudrait plus d'un an avant que cela arrive. c'est la même chose pour les coordonnées contenues dans Game.

## Textures chargées dynamiquement
Les Textures sont chargées à la première utilisation et non à l'initialisation, ça pourrait poser soucis si beaucoup d'Object différents étaient amenés à être chargés pour la première fois en même temps, en réalité ça n'arrive pas et n'a pas d'impact visible à l'affichage.

# Tests
Pas de bug connu actuellement.

Afin de faciliter le débogage, un fichier d'entêtes _debug.h_ a été mis à notre disposition, PRINT_DEBUG() permet de facilement rajouter des affichages lorsque l'option de debug associée est activée, ce qui est plus pratique que printf. La plupart des tests ont été réalisés à la main avec cette méthode, permetant ainsi de voir si les données étaient dans des états incohérents.

Il y a pas d'algorithme comprenant des calculs complexes à implémenter donc pas de tests unitaires à réliser, déterminer s'il y a intersection entre deux rectangles pour les collisions aurait pu être testé mais cela est réalisé par une fonction de SDL.

des cibles debug ont été ajoutées au makefile avec différents drapeaux de compilation et liaison. (l'exécutable est alors beaucoup plus lourd)

## Fuite de mémoire, segfault et autre
- valgrind avec --suppressions=\<fichier de suppression pour sdl\> ne trouve pas de fuite de mémoire.
- Utilisation de -fsanitize=address (gcc et linker) et lancer `ASAN_OPTIONS=fast_unwind_on_malloc=0 ./game` (beaucoup plus rapide que valgind)
- Utilisation de gdb


# Pistes d'améliorations

## Un thread pour le rendu graphique
On pourrait baser la mécanique du jeu sur le temps plutôt que sur la vitesse du rendu graphique. Une version simple permetant d'implémenter ce mécanisme serait de créer un thread pour le rendu graphique et remplacer l'affichage dans la boucle principale du jeu par un delay fixé. Il n'est pas problématique d'afficher le jeu alors que celui-ci est en train d'être mis à jour, bien que cela puisse afficher le jeu dans un état incohérent (potentiels "data race" entre les threads), cela va trop vite que pour que l'utilisateur puisse le voir. Le seul cas réellement problématique est lorsque un objet est supprimé de la liste chainée (segfault potentiel). Il serait donc nécessaire d'ajouter un mutex pour ce cas dans la structure Game.

Bien que meilleure cette implémentation n'est pas parfaite car le temps de mise à jour du jeu est ajoutée au delay dans la boucle principale. Cependant, celui-ci est actuellement inférieur à 1 ms. Une version basée sur des timer nécéciterait de créer en plus un thread pour la mise à jour du jeu avec une file de tâches à réaliser, ce qui est un peu plus complexe.
