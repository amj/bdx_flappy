#include <math.h>

#include "bad_bird.h"
#include "constants.h"
#include "debug.h"
#include "game.h"
#include "object.h"

static int BadBird__animation_onestep(BadBird* bird, Game* game) {
    bird->cycle++;
    int sign = bird->direction ? 1 : -1;
    bird->speed.y = cos(bird->cycle / 10) * 6;
    bird->position.y += bird->speed.y;
    bird->position.x += sign * bird->speed.x;

    if (bird->position.y >= (WIN_HEIGHT - MIN_BORDER_DISTANCE)) {
        bird->position.y = WIN_HEIGHT - MIN_BORDER_DISTANCE;
        bird->state = OBJECT_STATE_ON_GROUND;
    } else {
        bird->state = OBJECT_STATE_IN_AIR;
    }
    return (bird->parent == NULL);// || Object__is_outside(bird, game);
}

static Object* BadBird__collision(BadBird* bird, Object* other) {
    if (Object__is_solid(other) && other->type != OBJECT_TYPE_BAD_BIRD) {
        bird->parent = NULL;
        PRINT_DEBUG('c', "\nBadBird dead after collision %p\n", bird);
    }
    return NULL;
}

void badbird_class_init(object_class_t* class) {
    object_class_init(class);
    class->class_name = "BadBird";
    class->animate_onestep_func = &BadBird__animation_onestep;
    class->is_solid_func = &Object__true;
    class->collision_func = &BadBird__collision;
}
