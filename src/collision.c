#include <SDL.h>

#include "graphics.h"
#include "object.h"
#include "sprite.h"

#include "collision.h"

int collision_check(Graphics* graphics, Object* o1, Object* o2) {
    SDL_Rect dst1, dst2;
    
    Object__get_dst(o1, &dst1);
    Object__get_dst(o2, &dst2);
    return SDL_HasIntersection(&dst1, &dst2);
}
