#include <math.h>

#include "cursor.h"
#include "constants.h"
#include "debug.h"
#include "game.h"
#include "object.h"
#include "keys.h"



static int Cursor__animation_onestep(Cursor* cursor, Game* game) {
    return (cursor->parent == NULL);
}

static Object* Cursor__collision(Cursor* cursor, Object* other) {
    return NULL;
}

static void Cursor__action(Cursor* cursor, Game* game, int keys) {
    static int moveLock;
    static int tabLock;
    static static_object_type staticObjectSelected;
    static static_object_type background;

    //move the cursor
    if (moveLock < 1) {
        Map__set(Game__get_map(game), background, cursor->position.x / TILE_SIZE, cursor->position.y / TILE_SIZE);
        if (keys & KEY_UP)
            cursor->position.y -= TILE_SIZE;

        if (keys & KEY_DOWN)
            cursor->position.y += TILE_SIZE;

        if (keys & KEY_LEFT)
            cursor->position.x -= TILE_SIZE;

        if (keys & KEY_RIGHT)
            cursor->position.x += TILE_SIZE;
        background = Map__get(Game__get_map(game), cursor->position.x / TILE_SIZE, cursor->position.y / TILE_SIZE);
        Map__set(Game__get_map(game), staticObjectSelected, cursor->position.x / TILE_SIZE, cursor->position.y / TILE_SIZE);
        moveLock = 5;
    }
    else if (moveLock > 0) moveLock--;
    else if (!(keys & KEY_DOWN || keys & KEY_LEFT || keys & KEY_RIGHT || keys & KEY_UP)) moveLock = 0;

    //change the selected block
    if (tabLock < 1) {
        if (keys & KEY_TAB) {
            staticObjectSelected = (staticObjectSelected + 1) % __STATIC_OBJECT_TYPE_NUM;
            tabLock = 1;
        }
    }
    else if (!(keys & KEY_TAB))
        tabLock = 0;

    //place the selected block
    if (keys & KEY_SPACE)
        background = staticObjectSelected;
}

void cursor_class_init(object_class_t* class) {
    object_class_init(class);
    class->class_name = "Cursor";
    class->animate_onestep_func = &Cursor__animation_onestep;
    class->is_solid_func = &Object__false;
    class->collision_func = &Cursor__collision;
    class->action_func = &Cursor__action;
}
