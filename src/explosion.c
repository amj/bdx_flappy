#include "explosion.h"
#include "object.h"

static int Explosion__animation_onestep(Explosion* explosion, Game* game) {
    ++(explosion->cycle);
    explosion->position.x += explosion->speed.x;
    explosion->position.y += explosion->speed.y;
    if (explosion->speed.x != 0) explosion->speed.x /= 1.5;
    if (explosion->speed.y != 0) explosion->speed.y /= 1.5;

    return explosion->cycle >= 25;
}

void explosion_class_init(object_class_t* class) {
    object_class_init(class);
    class->class_name = "Explosion";
    class->animate_onestep_func = &Explosion__animation_onestep;
}
