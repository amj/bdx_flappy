#include "game.h"
#include "collision.h"
#include "constants.h"
#include "debug.h"
#include "error.h"
#include "graphics.h"

#include "object.h"
#include "object_elem.h"

static inline void Game__update_position(Game *game);

void Game__init(Game *game, Uint32 render_flags, char *background_skin, char *map) {
    game->cycle_nb = 0;
    game->position.x = 0;
    game->position.y = 0;

    game->graphics = Graphics__new(render_flags, background_skin);
    if (game->graphics == NULL) exit_with_error("Graphics__new");

    INIT_LIST_HEAD(&game->objects_list);

    game->visible_object = Object__new(game, OBJECT_TYPE_MARIO, TILE_SIZE * 2,
                                       TILE_SIZE * 2, 10, 0, 1, 2, 1);
    if (game->visible_object == NULL) exit_with_error("Object__new");
    Game__add_Object(game, game->visible_object);

    game->map = NULL;
    if (map != NULL) {
        game->map = Map__load(map);
    }


    if (game->map == NULL) {
        if (map != NULL) {
            perror(map);
        }
        game->map = Map__new(MAP_WIDTH, MAP_HEIGHT);
        Map__create(game->map);
    }

    Game__update_position(game);

    game->state = GAME_STATE_INIT;
}

void Game__clean(Game *game) {
    if (game->state != GAME_STATE_DESTROYED) {
        for_each_objects(game, obj_e) ObjectElem__delete(obj_e);

        if (game->graphics != NULL) Graphics__delete(game->graphics);
        if (game->map != NULL) Map__delete(game->map);
        game->state = GAME_STATE_DESTROYED;
    }
}

void Game__render_objects(Game *game) {
    Map__render(game->map, game->graphics, game->position.x, game->position.y,
                game->cycle_nb);
    for_each_objects(game, obj_e) Object__render(obj_e->obj, game);
}

void Game__render(Game *game) {
    Game__update_position(game);
    SDL_Rect map_rect;
    Map__get_rect(game->map, &map_rect);
    Graphics__render(game->graphics, game, map_rect.h);
}

#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define REC_E_X(r) (r->x + r->w)
#define REC_E_Y(r) (r->y + r->h)

// a must be smaller than b
static inline void a_contained_in_moved_b(const SDL_Rect *a, SDL_Rect *b) {
    b->x = MIN(b->x, a->x);
    b->y = MIN(b->y, a->y);
    if (REC_E_X(b) < REC_E_X(a)) b->x = REC_E_X(a) - b->w;
    if (REC_E_Y(b) < REC_E_Y(a)) b->y = REC_E_Y(a) - b->h;
}

// a must be smaller than b
static inline void move_a_contained_in_b(SDL_Rect *a, const SDL_Rect *b) {
    a->x = MAX(b->x, a->x);
    a->y = MAX(b->y, a->y);
    if (REC_E_X(b) < REC_E_X(a)) a->x = REC_E_X(b) - a->w;
    if (REC_E_Y(b) < REC_E_Y(a)) a->y = REC_E_Y(b) - a->h;
}

static inline void Game__get_visible_rect(const Game *game, SDL_Rect *rect) {
    rect->x = game->position.x;
    rect->y = game->position.y;
    Graphics__get_size(game->graphics, &rect->w, &rect->h);
}

static inline void Game__calcul_margin(const Game *game, int *x_margin,
                                       int *y_margin) {
    SDL_Rect visible, obj;
    Game__get_visible_rect(game, &visible);
    Object__get_dst(game->visible_object, &obj);
    *x_margin = MIN((visible.w - obj.w) / 2, MARGIN);
    *y_margin = MIN((visible.h - obj.h) / 2, MARGIN);
}

static inline void Game__get_center_rect(const Game *game, SDL_Rect *rect) {
    Game__get_visible_rect(game, rect);  // add margins
    int x_margin, y_margin;
    Game__calcul_margin(game, &x_margin, &y_margin);
    rect->x += x_margin;
    rect->y += y_margin;
    rect->w -= 2 * x_margin;
    rect->h -= 2 * y_margin;
}
static inline void Game__visible_from_center(const Game *game, SDL_Rect *visible,
                                       const SDL_Rect *center) {
    int x_margin, y_margin;
    Game__calcul_margin(game, &x_margin, &y_margin);
    visible->x = center->x - x_margin;
    visible->y = center->y - y_margin;
    visible->w = center->w + 2 * x_margin;
    visible->h = center->h + 2 * y_margin;
}

static inline void Game__update_position(Game *game) {
    SDL_Rect visible_rect, center_rect, obj_rect, map_rect;
    Map__get_rect(game->map, &map_rect);
    Object__get_dst(game->visible_object, &obj_rect);
    Game__get_center_rect(game, &center_rect);

    a_contained_in_moved_b(&obj_rect, &center_rect);
    Game__visible_from_center(game, &visible_rect, &center_rect);

    move_a_contained_in_b(&visible_rect, &map_rect);

    game->position.x = visible_rect.x;
    game->position.y = visible_rect.y;
}

long Game__next_cycle(Game *game, int keys) {
    for_each_objects(game, obj_e) {
        if(!Game__is_editor(game) || Object__get_type(obj_e->obj) == OBJECT_TYPE_EDITOR)
            Object__action(obj_e->obj, game, keys);
        else
            continue;
        for_each_objects_after(game, obj_e, sec_e) {
            if (collision_check(game->graphics, obj_e->obj, sec_e->obj)) {
                if (debug_enabled('c')) {
                    printf("collision: %s: %p %s: %p                  \r",
                           Object__get_name(obj_e->obj), obj_e->obj,
                           Object__get_name(sec_e->obj), sec_e->obj);
                    fflush(stdout);
                }
                Object *result = Object__collision(obj_e->obj, sec_e->obj);
                if (result != NULL) {
                    ObjectElem *elem = ObjectElem__new(result, &obj_e->listp);
                    if (elem == NULL) exit_with_error("ObjectElem__new");
                }
                result = Object__collision(sec_e->obj, obj_e->obj);
                if (result != NULL) {
                    ObjectElem *elem = ObjectElem__new(result, &obj_e->listp);
                    if (elem == NULL) exit_with_error("ObjectElem__new");
                }
            }
        }
        if (Object__animate(obj_e->obj, game)) ObjectElem__delete(obj_e);
    }
    Game__update_position(game);
    return Game__incr_cycle(game);
}

int Game__toggle_editor(Game *game) {
    if (game->state == GAME_STATE_STARTED) {
        PRINT_DEBUG('e', "enabling editor\n");
        game->state = GAME_STATE_EDITOR;
        for_each_objects(game, obj_e) Object__pause(obj_e->obj);

        Object* cursor = Object__new(game, OBJECT_TYPE_EDITOR, TILE_SIZE,
                            TILE_SIZE, 0, 0, 1, 1, 0);
        if (cursor == NULL) exit_with_error("Object__new");

        Game__add_Object(game, cursor);
        game->visible_object = cursor;
    } else if (game->state == GAME_STATE_EDITOR) {
        PRINT_DEBUG('e', "disabling editor\n");
        game->state = GAME_STATE_STARTED;

        for_each_objects(game, obj_e) {
            if (Object__get_type(obj_e->obj) == OBJECT_TYPE_EDITOR) {
                obj_e->obj->parent = NULL;
            } else {
                Object__unpause(obj_e->obj);
                if (Object__get_type(obj_e->obj) == OBJECT_TYPE_MARIO) {
                    game->visible_object = obj_e->obj;
                }
            }
        }
    } else
        return -1;
    return 0;
}