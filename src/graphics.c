#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>   // for NULL, sprintf, fflush, printf, stdout
#include <stdlib.h>  // for free, malloc

#include "constants.h"
#include "debug.h"
#include "error.h"
#include "graphics.h"

#include "game.h"
#include "object.h"
#include "sprite.h"
#include "static_object.h"

/// @private
typedef struct texture_info {
    char *filename;
    sprite_anim_type type;
    int nb_images;
    int width;
    int height;
    int direction;
} texture_info;

/// @private
static texture_info object_text_info[__OBJECT_TYPE_NUM] = {
    {"../images/mario.png", SPRITE_ANIM_SIMPLE, 11, 64, 128,
     SDL_FLIP_HORIZONTAL},  // mario
    {"../images/bad_bird.png", SPRITE_ANIM_YOYO, 8, 128, 128,
     SDL_FLIP_NONE},  // bird
    {"../images/missiled.png", SPRITE_ANIM_SIMPLE, 12, 64, 64,
     SDL_FLIP_HORIZONTAL},  // missile
    {"../images/explosion.png", SPRITE_ANIM_SIMPLE, 25, 64, 64,
     SDL_FLIP_NONE},  // explosion
    {"../images/cursor.png", SPRITE_ANIM_SIMPLE, 1, 96, 96,
     SDL_FLIP_NONE}
};

static texture_info static_object_text_info[__STATIC_OBJECT_TYPE_NUM - 1] = {
    {"../images/wall.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // wall
    {"../images/floor.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // floor
    {"../images/deep-water.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // water
    {"../images/flower.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // flower
    {"../images/flower2.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // flower2
    {"../images/grass.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // grass
    {"../images/ground.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // ground
    {"../images/herb.png", SPRITE_ANIM_SIMPLE, 1, 64, 64,
     SDL_FLIP_NONE},  // herb
    {"../images/coin.png", SPRITE_ANIM_SIMPLE, 16, 64, 64,
     SDL_FLIP_NONE},  // coin

};

void Graphics__init(Graphics *graphics, Uint32 render_flags,
                    char *background_skin) {
    graphics->state = GRAPHICS_STATE_UNINITIALIZED;
    graphics->win = NULL;
    graphics->ren = NULL;

    for (unsigned i = 0; i < __OBJECT_TYPE_NUM; ++i)
        graphics->sprites[i] = NULL;

    graphics->background = NULL;
    for (unsigned i = 0; i < 3; ++i) graphics->tree[i] = NULL;

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) != 0)
        exit_with_error("SDL_Init");

    graphics->state = GRAPHICS_STATE_INIT;

    // Create main window
    graphics->win = SDL_CreateWindow(
        "Battlefield Mario", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (graphics->win == NULL) exit_with_error("SDL_CreateWindow");
    /*SDL_SetWindowFullscreen(graphics->win, SDL_WINDOW_FULLSCREEN_DESKTOP);*/

    // Initialize graphical accelerated renderer
    graphics->ren = SDL_CreateRenderer(graphics->win, -1, render_flags);
    if (graphics->ren == NULL) exit_with_error("SDL_CreateRenderer");

    {
        char name[128];

        // Load background image and create the associated texture
        sprintf(name, "../images/%s-background.png", background_skin);
        graphics->background = IMG_LoadTexture(graphics->ren, name);
        if (graphics->background == NULL) exit_with_error("IMG_LoadTexture");

        // Load background trees
        for (int i = 0; i < 3; i++) {
            sprintf(name, "../images/%s-%d.png", background_skin, i);
            graphics->tree[i] = IMG_LoadTexture(graphics->ren, name);
            if (graphics->tree[i] == NULL) exit_with_error("IMG_LoadTexture");
        }
    }
}

static void Graphics__render_background(Graphics *graphics, SDL_Texture *tex,
                                        unsigned factor, long game_x,
                                        long game_y, int map_height) {
    SDL_Rect src, dst;
    int width, height, win_width, win_height;

    SDL_QueryTexture(tex, NULL, NULL, &width, &height);
    Graphics__get_size(graphics, &win_width, &win_height);

    src.x = 0;
    src.y = 0;
    src.w = width;
    src.h = height;

    width = (width * map_height) / height;

    if (factor == 0)
        dst.x = 0;
    else
        dst.x = -(game_x / factor) % width;
    dst.y = -game_y;
    dst.w = width;
    dst.h = map_height;

    do {
        SDL_RenderCopy(graphics->ren, tex, &src, &dst);
        dst.x += width;
    } while (dst.x < win_width);
}

void Graphics__render(Graphics *graphics, Game *game, int map_height) {
    static Uint32 prev = 0;
    Uint32 begin, end, interm;

    if (graphics->state != GRAPHICS_STATE_INIT)
        exit_with_error("bad graphic state");

    begin = SDL_GetTicks();

    // We clear the renderer's buffer
    SDL_RenderClear(graphics->ren);

    // We display the background clouds
    Graphics__render_background(graphics, graphics->background, 0, 0,
                                game->position.y, map_height);
    for (unsigned i = 0; i < 3; ++i)
        Graphics__render_background(graphics, graphics->tree[2 - i], 3 - i,
                                    game->position.x, game->position.y,
                                    map_height);


    Game__render_objects(game);

    interm = SDL_GetTicks();

    // We update the visible screen. SDL uses a double buffering, so
    // previous modifications are not yet visible
    SDL_RenderPresent(graphics->ren);

    end = SDL_GetTicks();

    if (debug_enabled('p')) {
        printf(
            "Animation: %2d, Rendering: %2d ms, VSync: %2d, Total: %2d ms)\r",
            begin - prev, interm - begin, end - interm, end - prev);
        fflush(stdout);
    }

    prev = end;
}

void Graphics__clean(Graphics *graphics) {
    if (graphics->state == GRAPHICS_STATE_UNINITIALIZED ||
        graphics->state == GRAPHICS_STATE_DESTROYED)
        return;

    if (graphics->ren != NULL) SDL_DestroyRenderer(graphics->ren);
    graphics->ren = NULL;

    if (graphics->win != NULL) SDL_DestroyWindow(graphics->win);
    graphics->win = NULL;

    for (unsigned i = 0; i < __OBJECT_TYPE_NUM; ++i) {
        if (graphics->sprites[i] != NULL) Sprite__delete(graphics->sprites[i]);
        graphics->sprites[i] = NULL;
    }

    if (graphics->background != NULL) SDL_DestroyTexture(graphics->background);
    graphics->background = NULL;

    for (int i = 0; i < 3; i++) {
        if (graphics->tree[i] != NULL) SDL_DestroyTexture(graphics->tree[i]);
        graphics->tree[i] = NULL;
    }

    IMG_Quit();
    SDL_Quit();
    graphics->state = GRAPHICS_STATE_DESTROYED;
}

static inline Sprite *Graphics__create_sprite(Graphics *graphics,
                                              texture_info tex_info) {
    SDL_Texture *tex = IMG_LoadTexture(graphics->ren, tex_info.filename);
    if (tex == NULL) exit_with_error("IMG_LoadTexture : %s", tex_info.filename);
    Sprite *sprite =
        Sprite__new(tex, tex_info.filename, tex_info.type, tex_info.width,
                    tex_info.height, tex_info.nb_images, tex_info.direction);
    if (sprite == NULL) exit_with_error("Sprite__new");
    return sprite;
}

Sprite *Graphics__get_sprite(Graphics *graphics, object_type type) {
    if (graphics->sprites[type] == NULL)
        graphics->sprites[type] =
            Graphics__create_sprite(graphics, object_text_info[type]);
    return graphics->sprites[type];
}

Sprite *Graphics__get_static_sprite(Graphics *graphics,
                                    static_object_type type) {
    if (graphics->static_sprites[type - 1] == NULL)
        graphics->static_sprites[type - 1] = Graphics__create_sprite(
            graphics, static_object_text_info[type - 1]);
    return graphics->static_sprites[type - 1];
}

void Graphics__render_object(Graphics *graphics, object_type type,
                             SDL_Rect *dst, int obj_cycle, int object_direction,
                             Uint8 alpha) {
    Sprite *sprite = Graphics__get_sprite(graphics, type);
    PRINT_DEBUG('s', "object type: %i \n", type);
    Sprite__render(sprite, graphics->ren, dst, obj_cycle, object_direction,
                   alpha);
}

void Graphics__render_static_object(Graphics *graphics, static_object_type type,
                                    int dst_x, int dst_y, int obj_cycle,
                                    int object_direction, Uint8 alpha) {
    if (type == 0) return;
    SDL_Rect dst;
    dst.x = dst_x;
    dst.y = dst_y;
    dst.w = TILE_SIZE;  // size of static objects is fixed
    dst.h = TILE_SIZE;
    Sprite *sprite = Graphics__get_static_sprite(graphics, type);
    PRINT_DEBUG('s', "static object type: %i \n", type);
    Sprite__render(sprite, graphics->ren, &dst, obj_cycle, object_direction,
                   alpha);
}
