#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"
#include "SDL_image.h"

#include "debug.h"
#include "game.h"
#include "keys.h"
#include "object.h"
#include "timer.h"

#define DEFAULT_BACKGROUND_SKIN "trees"

static char *progname;
static char *skin = NULL;
static char *map = NULL;

Game game;

// graphics from graphics.h;

void usage(int val) {
    fprintf(stderr, "Usage: %s [option]\n", progname);
    fprintf(stderr, "option can be:\n");
    fprintf(stderr, "\t-nvs\t| --no-vsync\t\t: disable vertical screen sync\n");
    fprintf(stderr,
            "\t-s\t| --skin <name>\t\t: use specific background skin\n");
    fprintf(stderr, "\t-d\t| --debug-flags <flags>\t: enable debug messages\n");
    fprintf(stderr, "\t-h\t| --help\t\t: display help\n");

    exit(val);
}

void clean(void) { Game__clean(&game); }

int main(int argc, char **argv) {
    Uint32 render_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
    char *debug_flags = NULL;

    progname = argv[0];

    // Filter args
    //
    argv++;
    argc--;
    while (argc > 0) {
        if (!strcmp(*argv, "--no-vsync") || !strcmp(*argv, "-nvs")) {
            render_flags &= ~SDL_RENDERER_PRESENTVSYNC;
        } else if (!strcmp(*argv, "--help") || !strcmp(*argv, "-h")) {
            usage(0);
        } else if (!strcmp(*argv, "--debug-flags") || !strcmp(*argv, "-d")) {
            if (argc == 1) {
                fprintf(stderr, "Error: flag list missing\n");
                usage(1);
            }
            argc--;
            argv++;
            debug_flags = *argv;
        } else if (!strcmp(*argv, "--skin") || !strcmp(*argv, "-s")) {
            if (argc == 1) {
                fprintf(stderr, "Error: skin name missing\n");
                usage(1);
            }
            argc--;
            argv++;
            skin = *argv;
        } else if (!strcmp(*argv, "--load") || !strcmp(*argv, "-l")) {
            if (argc == 1) {
                fprintf(stderr, "Error: map name missing\n");
                usage(1);
            }
            argc--;
            argv++;
            map = *argv;
        } else
            break;
        argc--;
        argv++;
    }
    

    if (argc > 0) usage(1);

    debug_init(debug_flags);

    object_classes_init();

    Game__init(&game, render_flags, (skin ? skin : DEFAULT_BACKGROUND_SKIN), map);

    if (map == NULL)
        map = DEFAULT_MAP_NAME;

    atexit(&clean);

    const Uint8 *keystates = SDL_GetKeyboardState(NULL);
    for (int quit = 0; !quit;) {
        SDL_Event evt;

        int keys = KEY_NONE | (keystates[SDL_SCANCODE_SPACE] ? KEY_SPACE : 0) |
                   (keystates[SDL_SCANCODE_UP] ? KEY_UP : 0) |
                   (keystates[SDL_SCANCODE_DOWN] ? KEY_DOWN : 0) |
                   (keystates[SDL_SCANCODE_LEFT] ? KEY_LEFT : 0) |
                   (keystates[SDL_SCANCODE_RIGHT] ? KEY_RIGHT : 0) |
                   (keystates[SDL_SCANCODE_TAB] ? KEY_TAB : 0);

        if (Game__is_init(&game) && keys) Game__start(&game);

        // We look for keyboard/mouse events (in a non-blocking way)
        while (SDL_PollEvent(&evt)) {
            switch (evt.type) {
                case SDL_QUIT:
                    // If user closes the window
                    quit = 1;
                    break;

                case SDL_USEREVENT:
                    switch (evt.user.code) {
                        case TIMER_OBJECT_EXP:
                            Object__timer_expires(evt.user.data1);
                            break;
                        default:
                            break;
                    }
                    break;

                case SDL_KEYDOWN:
                    // if user presses a key
                    switch (evt.key.keysym.sym) {
                        case SDLK_ESCAPE:
                            // ESC : we quit!
                            PRINT_DEBUG('k', "escape key pressed\n");
                            quit = 1;
                            break;
                        case SDLK_e:
                            PRINT_DEBUG('k', "editor key pressed\n");
                            Game__toggle_editor(&game);
                            PRINT_DEBUG('e', "finished toggling editor\n");
                            break;

                        case SDLK_s:
                            PRINT_DEBUG('k', "save key pressed\n");
                            Map__save(Game__get_map(&game), map);
                            break;
                        default:;
                    }
                    break;

                default:;
            }
        }
        // Refresh screen
        Game__render(&game);
        if (Game__is_started(&game) || Game__is_editor(&game)) Game__next_cycle(&game, keys);
    }

    return 0;
}
