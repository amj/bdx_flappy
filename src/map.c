#include <stdlib.h>

#include "debug.h"
#include "error.h"
#include "graphics.h"
#include "map.h"
#include "sprite.h"
#include "static_object.h"
#include "stdio.h"

typedef struct Map {
    static_object_type** objects;
    unsigned width;
    unsigned height;
} Map;

Map* Map__new(unsigned width, unsigned height) {
    Map* map = calloc(1, sizeof(Map));
    if (map != NULL) Map__init(map, width, height);
    return map;
}

int Map__is_valid(Map* map, unsigned x, unsigned y){
    return (x < map->width && y < map->height);
}

inline void Map__validate(Map* map, unsigned x, unsigned y) {
    if (x >= map->width || y >= map->height)
        exit_with_error("Map__validate: invalid map coordinates x %d y %d\n", x,
                        y);
}

inline void Map__set(Map* map, static_object_type map_object, unsigned x,
                     unsigned y) {
    Map__validate(map, x, y);
    map->objects[x][y] = map_object;
}

inline static_object_type Map__get(Map* map, unsigned x, unsigned y) {
    Map__validate(map, x, y);
    return map->objects[x][y];
}

static_object_type Map__safe_get(Map* map, unsigned x, unsigned y) {
    if(Map__is_valid(map, x, y))
        return Map__get(map,x,y);
    return STATIC_OBJECT_EMPTY;
}

inline void Map__render_at(Map* map, Graphics* graphics, int x_orig, int y_orig,
                           int game_cycle, unsigned x, unsigned y) {
    Graphics__render_static_object(graphics, Map__get(map, x, y),
                                   x * TILE_SIZE - x_orig,
                                   y * TILE_SIZE - y_orig, game_cycle, 0, 255);
}

void Map__render(Map* map, Graphics* graphics, int x_orig, int y_orig,
                 int game_cycle) {
    for (unsigned i = 0; i < map->width; ++i)
        for (unsigned j = 0; j < map->height; ++j) {
            Map__render_at(map, graphics, x_orig, y_orig, game_cycle, i, j);
        }
}

void Map__create(Map* map) {  // have to be replaced by map__load
    Map__set(map, STATIC_OBJECT_COIN, 21, 21);
    for (int i = 0; i < map->width; i++)
        Map__set(map, STATIC_OBJECT_GROUND, i, map->height - 1);
    for (int i = 0; i < map->height - 1; i++)
        Map__set(map, STATIC_OBJECT_WALL, 0, i);
    // Map__set(map, STATIC_OBJECT_GROUND, 7, 15);
    // Map__set(map, STATIC_OBJECT_GROUND, 8, 15);
    // Map__set(map, STATIC_OBJECT_GROUND, 9, 15);
    // Map__set(map, STATIC_OBJECT_GROUND, 11, 15);
    // Map__set(map, STATIC_OBJECT_GROUND, 12, 15);
    // Map__set(map, STATIC_OBJECT_WALL, 10, 20);
    // Map__set(map, STATIC_OBJECT_WALL, 10, 21);
    // Map__set(map, STATIC_OBJECT_WALL, 10, 22);
    // Map__set(map, STATIC_OBJECT_WALL, 10, 23);
}

Map* Map__load(char* filename) {
    PRINT_DEBUG('m', "loading map from file '%s'\n", filename);
    FILE* map_file = fopen(filename, "r");
    if (map_file == NULL) return NULL;
    int height, width;
    int error = (fread(&height, sizeof(height), 1, map_file) < 1) ||
                (fread(&width, sizeof(width), 1, map_file) < 1);

    Map* map = calloc(1, sizeof(Map));
    if (map != NULL) {
        Map__init(map, width, height);
        for (unsigned i = 0; !error && i < width; ++i) {
            PRINT_DEBUG('m', "loading column %d/%d\n", i + 1, width);
            error |= (fread(map->objects[i], sizeof(**map->objects), height,
                            map_file) < height);
        }
    }
    fclose(map_file);
    if (error && map != NULL) {
        Map__delete(map);
        return NULL;
    }
    return map;
}

int Map__save(Map* map, char* filename) {
    PRINT_DEBUG('m', "saving map to file '%s'\n", filename);
    FILE* map_file = fopen(filename, "w+");
    if (map_file == NULL) return 0;
    int error = (fwrite(&map->height, sizeof(map->height), 1, map_file) < 1) ||
                (fwrite(&map->width, sizeof(map->width), 1, map_file) < 1);

    for (unsigned i = 0; !error && i < map->width; ++i) {
        PRINT_DEBUG('m', "saving column %d/%d\n", i + 1, map->width);
        error |= (fwrite(map->objects[i], sizeof(**map->objects), map->height,
                         map_file) < map->height);
    }
    fclose(map_file);
    return !error;
}

void Map__init(Map* map, unsigned width, unsigned height) {
    map->width = width;
    map->height = height;

    map->objects = malloc(width * sizeof(static_object_type*));
    if (map->objects == NULL) {
        exit_with_error("unable to allocate map grid");
    }

    for (unsigned i = 0; i < width; i++) {
        map->objects[i] = calloc(height, sizeof(static_object_type));
        if (map->objects[i] == NULL) {
            exit_with_error("unable to allocate map grid");
        }
    }
}

void Map__clean(Map* map) {
    for (int i = 0; i < map->width; i++)
        if (map->objects[i] != NULL) free(map->objects[i]);
    free(map->objects);
}

void Map__delete(Map* map) {
    Map__clean(map);
    free(map);
}

void Map__get_rect(Map* map, SDL_Rect* rect) {
    rect->x = 0;
    rect->y = 0;
    rect->h = map->height * TILE_SIZE;
    rect->w = map->width * TILE_SIZE;
}

