#include <stdlib.h>

#include "constants.h"
#include "debug.h"
#include "error.h"
#include "game.h"
#include "keys.h"
#include "map.h"
#include "mario.h"
#include "object.h"
#include "static_collision.h"

inline void die(Mario* mario) {
    mario->is_dead = 1;
    Object__timer_add(mario, DEAD_DELAY);
}

static int Mario__animation_onestep(Mario* mario, Game* game) {
    if (mario->speed.x != 0) ++(mario->cycle);
    mario->position.y += mario->speed.y;

    //collisions (here be dragons)
    //vertical collisisons
    if (mario->speed.y > 0 && checkLowerCollision(mario->position.x, mario->position.y, mario->size.x, mario->size.y, game)) {
        mario->speed.y = 0;
        mario->position.y = mario->position.y / TILE_SIZE * TILE_SIZE;
        mario->state = OBJECT_STATE_ON_GROUND;
    }
    else {
        mario->state = OBJECT_STATE_IN_AIR;
    }

    if (mario->speed.y < 0 && checkUpperCollision(mario->position.x, mario->position.y, mario->size.x, game)) {
        mario->speed.y = 0;
        mario->position.y = (mario->position.y / TILE_SIZE + 1) * TILE_SIZE;
    }

    mario->position.x += mario->speed.x;

    //horizontal collisions
    if (mario->speed.x > 0 && checkRightCollision(mario->position.x, mario->position.y, mario->size.x, mario->size.y, game)) {
        mario->position.x = mario->position.x / TILE_SIZE * TILE_SIZE;;
        mario->speed.x = 0;
    } else if (mario->speed.x < 0 && checkLeftCollision(mario->position.x, mario->position.y, mario->size.y, game)) {
        mario->position.x = (mario->position.x / TILE_SIZE + 1) * TILE_SIZE;;
        mario->speed.x = 0;
    }

    if (mario->speed.y < MAX_SPEED) mario->speed.y += 1;

    if (Object__is_out(mario, Game__get_map(game))){
        mario->position.x = TILE_SIZE;
        mario->position.y = TILE_SIZE;
    }
    return 0;
}

static inline void Mario__shoot(Mario* mario, Game* game) {
    Object* shoot = Object__new(mario, OBJECT_TYPE_MISSILE, mario->position.x,
                                mario->position.y, mario->speed.x, 0, 1, 1,
                                mario->direction);
    if (shoot == NULL) exit_with_error("Object__new");
    Game__add_Object(game, shoot);
}

static void Mario__action(Mario* mario, Game* game, int keys) {
    if (!mario->is_dead) {
        if ((keys & KEY_UP) && mario->state == OBJECT_STATE_ON_GROUND)
            mario->speed.y = -MAX_SPEED;

        if ((keys & KEY_RIGHT)) {
            if (mario->speed.x < MAX_SPEED / 2) mario->speed.x += 4;
            mario->direction = 1;
        } else if ((keys & KEY_LEFT)) {
            if (mario->speed.x > -MAX_SPEED / 2) mario->speed.x -= 4;
            mario->direction = 0;
        } else {
            mario->speed.x = 0;
        }

        if (mario->buff < 1) {
            if (keys & KEY_SPACE) {
                Mario__shoot(mario, game);
                mario->buff = 25;
            }
        }
        else if (mario->buff > 0) mario->buff--;
        else if (!(keys & KEY_SPACE)) mario->buff = 0;
    }
}

static void Mario__timer_expires(Mario* mario) {
    PRINT_DEBUG('t', "Bird timer expire\n");
    if (!mario->is_dead)
        exit_with_error("invalid object state at timer expiration\n");
    mario->is_dead = 0;
}

static Object* Mario__collision(Mario* mario, Object* other) {
    if (other->parent != mario && Object__is_solid(other)) die(mario);
    return NULL;
}

void mario_class_init(object_class_t* class) {
    object_class_init(class);
    class->class_name = "Mario";
    class->animate_onestep_func = &Mario__animation_onestep;
    class->action_func = &Mario__action;
    class->timer_func = &Mario__timer_expires;
    class->is_solid_func = &Object__true;
    class->collision_func = &Mario__collision;
}
