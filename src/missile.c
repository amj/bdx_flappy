#include "missile.h"
#include "debug.h"
#include "game.h"
#include "object.h"
#include "static_collision.h"

inline void Missile__explode(Missile* missile) {
    Object__init(missile, missile->parent, OBJECT_TYPE_EXPLOSION,
                 missile->position.x, missile->position.y, missile->speed.x,
                 missile->speed.y, missile->size.x, missile->size.y,
                 missile->direction);
}

inline Object* Missile__new_explosion(Missile* missile) {
    Object* explosion = malloc(sizeof(Object));
    if (explosion == NULL) exit_with_error("malloc");
    Object__init(explosion, missile->parent, OBJECT_TYPE_EXPLOSION,
                 missile->position.x, missile->position.y, missile->speed.x,
                 missile->speed.y, missile->size.x, missile->size.y,
                 missile->direction);
    return explosion;
}

static int Missile__animation_onestep(Missile* missile, Game* game) {
    ++(missile->cycle);
    if (missile->direction) {
        if (missile->speed.x < 20)
            missile->speed.x += 1;
    } else {
        if (missile->speed.x > -20)
            missile->speed.x -= 1;
    }
    missile->position.x += missile->speed.x;
    missile->position.y += missile->speed.y;

    //check out of bound
    SDL_Rect mis;
    mis.x = missile->position.x;
    mis.y = missile->position.y;
    mis.w = missile->size.x;
    mis.h = missile->size.y;
    SDL_Rect map;
    Map__get_rect(game->map, &map);
    if (!SDL_HasIntersection(&mis, &map)) {
        return 1;
    }
    
    //check collisions
    if(missile->speed.x > 0 && checkRightCollision(missile->position.x, missile->position.y, missile->size.x, missile->size.y, game)) {
        missile->speed.x = 0;
        Missile__explode(missile);
    }

    if(missile->speed.x < 0 && checkLeftCollision(missile->position.x, missile->position.y, missile->size.y, game)) {
        missile->speed.x = 0;
        Missile__explode(missile);
    }

    if (Object__is_out (missile, Game__get_map(game)))
        return 1;

    return missile->parent == NULL;
}

static Object* Missile__collision(Missile* missile, Object* other) {
    if (missile->parent != other && missile->parent != other->parent &&
        Object__is_solid(other)) {
        PRINT_DEBUG('c', "\nMissile explode after collision %p\n", missile);
        Object* o = Missile__new_explosion(missile);
        missile->parent = NULL;
        return o;
    }
    return NULL;
}

void missile_class_init(object_class_t* class) {
    object_class_init(class);
    class->class_name = "Missile";
    class->animate_onestep_func = &Missile__animation_onestep;
    class->is_solid_func = &Object__true;
    class->collision_func = &Missile__collision;
}

