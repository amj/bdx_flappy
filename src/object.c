
#include <math.h>

#include "bad_bird.h"
#include "constants.h"
#include "cursor.h"
#include "explosion.h"
#include "game.h"
#include "graphics.h"
#include "map.h"
#include "mario.h"
#include "missile.h"
#include "object.h"
#include "sprite.h"

object_class_t object_classes[__OBJECT_TYPE_NUM];

void object_classes_init(void) {
    mario_class_init(&object_classes[OBJECT_TYPE_MARIO]);
    badbird_class_init(&object_classes[OBJECT_TYPE_BAD_BIRD]);
    missile_class_init(&object_classes[OBJECT_TYPE_MISSILE]);
    explosion_class_init(&object_classes[OBJECT_TYPE_EXPLOSION]);
    cursor_class_init(&object_classes[OBJECT_TYPE_EDITOR]);
}

int Object__is_out(Object *object, Map *map) {
    return !(object->position.x >= 0 && object->position.y >= 0 &&
             Map__is_valid(map, object->position.x / TILE_SIZE,
                           object->position.y / TILE_SIZE));
}

void Object__init(Object *object, void *parent, object_type type, int x, int y,
                  int xs, int ys, int x_size, int y_size, int direction) {
    object->type = type;
    object->parent = parent;
    object->state = OBJECT_STATE_NORMAL;
    object->is_dead = 0;
    object->size.x = x_size;
    object->size.y = y_size;
    object->position.x = x;
    object->position.y = y;
    object->speed.x = xs;
    object->speed.y = ys;
    object->direction = direction;
    object->cycle = 0;
    object->buff = 0;
}

void Object__render(Object *object, Game *game) {
    Graphics *graphics = Game__get_Graphics(game);

    int game_x, game_y;
    Game__get_orig(game, &game_x, &game_y);

    SDL_Rect dst;
    Object__get_dst(object, &dst);
    dst.x = dst.x - game_x;
    dst.y = dst.y - game_y;

    Graphics__render_object(
        graphics, object->type, &dst, object->cycle, object->direction,
        object->is_dead ? 127 + 64 + sin(game->cycle_nb / 10) * 64 : 255);
}

void Object__timer_add(Object *object, Uint32 delay) {
    Object__timer_cancel(object);
    object->timer = Timer_set(delay, object);
}
