#include <stddef.h>

#include "debug.h"
#include "sprite.h"

void Sprite__init(Sprite *sprite, SDL_Texture *texture, char *text_path,
                  sprite_anim_type type, int x_size, int y_size, int nb_images,
                  int direction) {
    int tex_width, tex_height;
    SDL_QueryTexture(texture, NULL, NULL, &tex_width, &tex_height);

    sprite->texture = texture;
    sprite->text_path = text_path;
    sprite->type = type;

    sprite->element_size.y = y_size;
    sprite->element_size.x = x_size;

    sprite->native_size.y = tex_height;
    sprite->native_size.x = tex_width;

    sprite->nb_images = nb_images;

    sprite->original_direction = direction;
}

inline int sequence_simple(Sprite *sprite, int cycle) {
    if (sprite->nb_images == 1) return 0;
    return cycle % sprite->nb_images;
}

inline int sequence_yoyo(Sprite *sprite, int cycle) {
    if (sprite->nb_images == 1) return 0;

    int seq_nb =
        (cycle / 2) % (2 * (sprite->nb_images - 1));  //{0..n-1} {n-2..1}
    if (seq_nb >= sprite->nb_images)
        seq_nb = 2 * (sprite->nb_images - 1) - seq_nb;
    // printf("bird sequence: %i\n", seq_nb);
    return seq_nb;
}

inline int get_sequence(Sprite *sprite, int cycle) {
    switch (sprite->type) {
        case SPRITE_ANIM_SIMPLE:
            return sequence_simple(sprite, cycle);
        case SPRITE_ANIM_YOYO:
            return sequence_yoyo(sprite, cycle);
        default:
            return 0;
    }
}

/**
 * Get the src and dst Rectangle needed to apply the correct part of the texture
 * to the renderer
 */
static inline void Sprite__calcul_render(Sprite *sprite, unsigned obj_cycle,
                                         SDL_Rect *src, int *direction) {
    int seq_number = get_sequence(sprite, obj_cycle);
    int nb_width = sprite->native_size.x / sprite->element_size.x;
    int ind_width = seq_number % nb_width;
    int ind_heigth = seq_number / nb_width;
    PRINT_DEBUG('s', "nb w : %i h ind: %i w ind: %i \n", nb_width, ind_width,
                ind_heigth);

    if (src != NULL) {
        src->x = ind_width * sprite->element_size.x;
        src->y = ind_heigth * sprite->element_size.y;
        src->h = sprite->element_size.y;
        src->w = sprite->element_size.x;
    }

    if (direction != NULL) *direction = sprite->original_direction;
}

void Sprite__render(Sprite *sprite, SDL_Renderer *ren, SDL_Rect *dst,
                    int obj_cycle, int object_direction, Uint8 alpha) {
    SDL_Rect src;
    int direction;
    Sprite__calcul_render(sprite, obj_cycle, &src, &direction);
    PRINT_DEBUG('s', "Sprite: %s src: x: %i  y: %i h: %i w: %i\n",
                sprite->text_path, src.x, src.y, src.h, src.w);
    PRINT_DEBUG('s', "Sprite: %s dst: x: %i  y: %i h: %i w: %i\n",
                sprite->text_path, dst->x, dst->y, dst->h, dst->w);
    SDL_SetTextureAlphaMod(Sprite__get_texture(sprite), alpha);
    SDL_RenderCopyEx(ren, Sprite__get_texture(sprite), &src, dst, 0, 0,
                     (direction ^ object_direction));
}

void Sprite__clean(Sprite *sprite) {
    if (sprite->texture != NULL) SDL_DestroyTexture(sprite->texture);
}

