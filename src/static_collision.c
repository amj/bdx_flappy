#include "static_collision.h"

char checkLowerCollision(int x, int y, int objectWidth, int objectHeight, Game* game) {
    int mapx = x / TILE_SIZE;
    int mapy = y / TILE_SIZE;
    if (x % TILE_SIZE == 0)
        objectWidth -= 1;
    static_object_property toCheck = y > mapy * TILE_SIZE + 16 ?
        STATIC_PROPERTY_SOLID : STATIC_PROPERTY_SOLID | STATIC_PROPERTY_JUMP_THROUGH;
    for (int i = mapx; i < mapx + objectWidth + 1; i++) {
        if(static_object__has_property(Map__safe_get(game->map, i, mapy + objectHeight), toCheck))
            return 1;
    }
    return 0;
}

char checkUpperCollision(int x, int y, int objectWidth, Game* game) {
    int mapx = x / TILE_SIZE;
    int mapy = y / TILE_SIZE;
    if (x % TILE_SIZE == 0)
        objectWidth -= 1;
    for (int i = mapx; i < mapx + objectWidth + 1; i++) {
        if(static_object__has_property(Map__safe_get(game->map, i, mapy), STATIC_PROPERTY_SOLID))
            return 1;
    }
    return 0;
}

char checkRightCollision(int x, int y, int objectWidth, int objectHeight, Game* game) {
    int mapx = x / TILE_SIZE;
    int mapy = y / TILE_SIZE;
    if (y % TILE_SIZE == 0)
        objectHeight -= 1;
    for (int i = mapy; i < mapy + objectHeight + 1; i++) {
        if(static_object__has_property(Map__safe_get(game->map, mapx + objectWidth, i), STATIC_PROPERTY_SOLID))
            return 1;
    }
    return 0;
}

char checkLeftCollision(int x, int y, int objectHeight, Game* game) {
    int mapx = x / TILE_SIZE;
    int mapy = y / TILE_SIZE;
    if (y % TILE_SIZE == 0)
        objectHeight -= 1;
    for (int i = mapy; i < mapy + objectHeight + 1; i++) {
        if(static_object__has_property(Map__safe_get(game->map, mapx, i), STATIC_PROPERTY_SOLID))
            return 1;
    }
    return 0;
}
